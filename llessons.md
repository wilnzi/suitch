# Fichier de lessons apprises par rapport à ce projet

## Gestion des Annotations
Au travers de ce lien, on montre les différentes méthodes de validation possible des arguements.
La validation peut se faire aussi bien par code que par annotation.
Lien de lecture : [https://www.baeldung.com/javax-validation](https://www.baeldung.com/javax-validation)

## Pour resoudre l'erreur No Persistence provider for EntityManager named cm.wilnzi.suitch.model.user.User
1. [https://www.baeldung.com/hibernate-entitymanager](https://www.baeldung.com/hibernate-entitymanager)
1. [https://www.baeldung.com/hibernate-no-persistence-provider](https://www.baeldung.com/hibernate-no-persistence-provider)

En fait il n'etait meme pas nécessaire d'utiliser les EntityManager à ce niveau. Juste un customQuery de Spring Data JPA suffisait

## Pour un update avec Spring Data Jpa
https://www.baeldung.com/spring-data-jpa-modifying-annotation

### Lorsqu'on fait un update, il faut marquer la methode @Transactionnal