/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cm.wilnzi.suitch.utils;

import java.util.UUID;
import java.util.zip.CRC32;

/**
 *
 * @author WILFRIED
 */
public class Utils {

    public static String hash(String text) {
        CRC32 crc32 = new CRC32();
        crc32.update(text.getBytes());
        return Long.toHexString(crc32.getValue()).toUpperCase();
    }

    public static boolean compare(String clearText, String hash) {
        return hash.toUpperCase().equals(hash(clearText).toUpperCase());
    }

    public static String generateUUID() {
        UUID uuid = UUID.randomUUID();
        return uuid.toString().replaceAll("-", "");
    }

}
