package cm.wilnzi.suitch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SuitchApplication {

	public static void main(String[] args) {
		SpringApplication.run(SuitchApplication.class, args);
	}

}
