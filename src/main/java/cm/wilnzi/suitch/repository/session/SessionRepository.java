/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cm.wilnzi.suitch.repository.session;
 
import cm.wilnzi.suitch.model.session.Session;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


/**
 *
 * @author WILFRIED
 */


@Repository 
public interface SessionRepository extends JpaRepository<Session, Integer>{
    
    public Session findByToken(String token); 
    
    @Query(value = "CALL getSessionByPseudo(:pseudo);", nativeQuery = true)
    Session findByPseudo(@Param("pseudo") String pseudo);
    
}
