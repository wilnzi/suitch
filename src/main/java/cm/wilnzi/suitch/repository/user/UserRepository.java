/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cm.wilnzi.suitch.repository.user;

import cm.wilnzi.suitch.model.user.User;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

/**
 *
 * @author WILFRIED
 */

public interface UserRepository extends JpaRepository<User, Integer> {
    
    public User findByPseudoAndActivationStatus(String pseudo, int activationStatus);
 
//    public List<User> findAllAndActivationStatus(int activationStatus);
    public List<User> findAll();
    

    @Override
    public User save(User user);
    
    @Transactional
    @Modifying(clearAutomatically = true)
    @Query(value = "UPDATE shuser set usacti = 0, usdmaj = NOW(), usumaj = 'SHADMIN' WHERE uscode = :pseudo AND usacti = 1;", nativeQuery = true)
    public void customDelete(String pseudo);
 
//    @Transactional
//    @Modifying
//    @Query(name = "UPDATE shuser SET usacti = 0, usdmaj = NOW(), usumaj = 'SHADMIN' WHERE ususer = 'SQFDSQFSDQF'", nativeQuery = true)
//    public void updateActivationStatus (String pseudo);
    
}
