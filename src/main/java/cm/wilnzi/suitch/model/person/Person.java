/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cm.wilnzi.suitch.model.person;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table; 

/**
 *
 * @author WILFRIED
 */
@Entity
@Table(name = "shpers") 
public class Person implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "shpersid") 
    private Integer id ;

    @Column(name = "penomf")
    private String name; 
    
    @Column(name = "penomp")
    private String firstName; 
    
   
    @Column(name = "penomd")
    private String displayName;

    @Column(name = "petele")
    private String phoneNumber;

    @Override
    public String toString() {
        return "Person{" + "id=" + id + ", name=" + name + ", firstName=" + firstName + ", displayName=" + displayName + ", phoneNumber=" + phoneNumber + '}';
    }    
    
     public void setDisplayName(){
        this.displayName = (name == null ? "" : name) + " "+(firstName == null ? "" : firstName);
    }

    public Integer getId() {
        return id;
    }
 

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getDisplayName() {
        return displayName;
    }

   

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Person other = (Person) obj;
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }
     
    
     
    
}
