/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cm.wilnzi.suitch.model.headerinformation;

import cm.wilnzi.suitch.model.session.Session;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

/**
 *
 * @author WILFRIED
 */

@Component
@Getter
@Setter

public class HeaderInformation {
    
    private Session session;
    private String headerPseudoUser;
    private String headerPassword;
    
}
