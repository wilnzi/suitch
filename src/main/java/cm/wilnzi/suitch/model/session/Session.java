/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cm.wilnzi.suitch.model.session;

import cm.wilnzi.suitch.model.user.User;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author WILFRIED
 */
@Entity
@Table(name = "shsess")
@Getter
@Setter

public class Session {

    public static final int TOKEN_DURATION = 10;
    
    
    @JsonIgnore
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Setter(AccessLevel.NONE)
    @Column(name = "shsessid")
    private Integer id;

    @Column(name = "sstken")
    private String token;

    @JsonIgnore
    @OneToOne
    @JoinColumn(name = "ssuser", referencedColumnName = "uscode")
    private User user;

    @Column(name = "ssddeb")
//    @Temporal(TemporalType.TIMESTAMP)
    private LocalDateTime startDateTime;

    @Column(name = "ssdfin")
//    @Temporal(TemporalType.TIMESTAMP)
    private LocalDateTime endDateTime;

    @Override
    public String toString() {
        return "Session{" + "id=" + id + ", token=" + token + ", user=" + user + ", startDateTime=" + startDateTime + ", endDateTime=" + endDateTime + '}';
    }

}
