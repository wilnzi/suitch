/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cm.wilnzi.suitch.model.user;

/**
 *
 * @author WILFRIED
 */
import cm.wilnzi.suitch.model.person.Person;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.sun.istack.NotNull;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "shuser")

public class User implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "shuserid")
    private Integer id;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "uspers", referencedColumnName = "shpersid")
//    @JsonProperty("newPerson") permet de changer le nom de cet attribut dans le json sortant
    private Person person;

    @Column(name = "uscode", updatable = false, nullable = false)
    private String pseudo;

    @Column(name = "uspass")
    private String password;

    @Override
    public String toString() {
        return "User{" + "id=" + id + ", person=" + (person == null ? "null" : person.toString()) + ", pseudo=" + pseudo + ", password=" + password + '}';
    }

    @JsonIgnore
    @Column(name = "usacti")
    private int activationStatus = 1;

    public Integer getId() {
        return id;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public String getPseudo() {
        return pseudo;
    }

    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getActivationStatus() {
        return activationStatus;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + Objects.hashCode(this.id);
        hash = 23 * hash + Objects.hashCode(this.person);
        hash = 23 * hash + Objects.hashCode(this.pseudo);
        hash = 23 * hash + Objects.hashCode(this.password);
        hash = 23 * hash + this.activationStatus;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final User other = (User) obj;

        if (!Objects.equals(this.pseudo, other.pseudo)) {
            return false;
        }

        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        if (!Objects.equals(this.person, other.person)) {
            return false;
        }
        return true;
    }

}
