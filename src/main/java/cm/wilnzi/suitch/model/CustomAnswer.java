/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cm.wilnzi.suitch.model;

import cm.wilnzi.suitch.model.session.Session;
import com.sun.el.stream.Optional;
import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

/**
 *
 * @author WILFRIED
 * @param <T>
 */
@Component
@Getter
@Setter
public class CustomAnswer<T extends Object> implements Serializable {

//    private HttpStatus httpStatus;
    private boolean hasError = false;
    private T content;
    private String errorMessage; 
    private Session session;

   public void setErrorMessage(String errorMessage){
       hasError = true;
       if(!(errorMessage == null ||  errorMessage.trim().equals(""))){
           this.errorMessage = errorMessage;
           
       }
   }

}
