/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cm.wilnzi.suitch.exception.headerinformation;

/**
 *
 * @author WILFRIED
 */
public class NoHeaderInformationException extends Exception{
    
    public NoHeaderInformationException(String message){
        super(message);
    }
    
}
