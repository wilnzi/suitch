/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cm.wilnzi.suitch.controller.user;

import cm.wilnzi.suitch.controller.AbstractCrudController;
import cm.wilnzi.suitch.controller.CrudController;
import cm.wilnzi.suitch.model.CustomAnswer;
import cm.wilnzi.suitch.model.headerinformation.HeaderInformation;
import cm.wilnzi.suitch.model.user.User;
import cm.wilnzi.suitch.service.headerinformation.HeaderInformationServiceImpl;
import cm.wilnzi.suitch.exception.headerinformation.NoHeaderInformationException;
import cm.wilnzi.suitch.exception.headerinformation.SessionManipulationException;
import cm.wilnzi.suitch.service.user.UserService;
import cm.wilnzi.suitch.service.user.UserServiceImpl;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.server.ResponseStatusException;

/**
 *
 * @author WILFRIED
 */
@RestController
public class UserControllerImpl extends AbstractCrudController implements CrudController<String, User> {

    @Autowired
    UserService userService;

    @Autowired
    HeaderInformationServiceImpl headerInformationServiceImpl;

    @Override
    @GetMapping(value = "/user/{pseudo}")
    public ResponseEntity<CustomAnswer<User>> get(@RequestHeader Map<String, String> headers, @PathVariable String pseudo) {
        HeaderInformation headerInformation;
        CustomAnswer customAnswer = new CustomAnswer();
        try {
            headerInformation = init(headers);
            customAnswer.setSession(headerInformation.getSession());
        } catch (Exception e) {
            customAnswer.setErrorMessage(e.getMessage());
            return new ResponseEntity<>(customAnswer, HttpStatus.BAD_REQUEST);
        }

        Optional<User> optional = Optional.ofNullable(userService.get(pseudo));
        if (optional.isPresent()) {
            customAnswer.setContent(optional.get());
            return new ResponseEntity<>(customAnswer, HttpStatus.OK);
        } else {
            customAnswer.setErrorMessage("Not preent user ");
            return new ResponseEntity<>(customAnswer, HttpStatus.NOT_FOUND);
        }

    }

    @Override
    @GetMapping(value = "/user")
    public ResponseEntity<CustomAnswer<List<User>>> getAll(@RequestHeader Map<String, String> headers) {
        CustomAnswer customAnswer = new CustomAnswer();
        HeaderInformation headerInformation;
        try {
            headerInformation = init(headers);
            customAnswer.setSession(headerInformation.getSession());
        } catch (Exception e) {
            customAnswer.setErrorMessage(e.getMessage());
            return new ResponseEntity<>(customAnswer, HttpStatus.BAD_REQUEST);
        }

        customAnswer.setSession(headerInformation.getSession());

        Optional<List<User>> optional = Optional.ofNullable(userService.getAll());
        customAnswer.setContent(optional.orElse(new LinkedList<>()));
        return new ResponseEntity<>(customAnswer, HttpStatus.OK);

    }

    @Override
    @PostMapping(value = "/user/")
    public ResponseEntity<CustomAnswer<User>> add(@RequestBody User user) {
        CustomAnswer customAnswer = new CustomAnswer();
        if (user == null || user.getPassword() == null) {
            customAnswer.setErrorMessage("JSON Utilisateur non conforme. Verifier les informations saisies");
            return new ResponseEntity<>(customAnswer, HttpStatus.BAD_REQUEST);

        }
        Optional<User> optional = Optional.ofNullable(userService.add(user));

        if (optional.isPresent()) {
            customAnswer.setContent(optional.get());
            return new ResponseEntity<>(customAnswer, HttpStatus.OK);
        } else {
            customAnswer.setErrorMessage("Impossible d'enregistrer cet utilisateur");
            return new ResponseEntity<>(customAnswer, HttpStatus.BAD_REQUEST);

        }
    }

    @Override
    @PatchMapping(value = "/user/{pseudo}")
    public ResponseEntity<CustomAnswer<User>> update(@RequestHeader Map<String, String> headers, @PathVariable String pseudo, @Valid @RequestBody User user) {
        CustomAnswer customAnswer = new CustomAnswer();
        HeaderInformation headerInformation;
        try {
            headerInformation = init(headers);
            customAnswer.setSession(headerInformation.getSession());
        } catch (Exception ex) {
            Logger.getLogger(UserControllerImpl.class.getName()).log(Level.SEVERE, null, ex);
            customAnswer.setErrorMessage(ex.getMessage());
            return new ResponseEntity<>(customAnswer, HttpStatus.BAD_REQUEST);
        }

        customAnswer.setSession(headerInformation.getSession());

        Optional<User> optional;
        try {
            optional = Optional.ofNullable(userService.update(pseudo, user));
            if (!optional.isPresent()) {
                customAnswer.setErrorMessage("Erreur lors de la mise à jour de l'utilisateur");
                return new ResponseEntity<>(customAnswer, HttpStatus.INTERNAL_SERVER_ERROR);
            }
            customAnswer.setContent(optional.get());
            return new ResponseEntity<>(customAnswer, HttpStatus.OK);
        } catch (Exception ex) {
            Logger.getLogger(UserControllerImpl.class.getName()).log(Level.SEVERE, null, ex);
            customAnswer.setErrorMessage(ex.getMessage());
            return new ResponseEntity<>(customAnswer, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @Override
    @DeleteMapping(value = "/user/{pseudoUser}")
    public ResponseEntity<CustomAnswer<User>> delete(@RequestHeader Map<String, String> headers, @PathVariable String pseudoUser) {
        CustomAnswer customAnswer = new CustomAnswer();
        HeaderInformation headerInformation;
        try {
            headerInformation = init(headers);
            customAnswer.setSession(headerInformation.getSession());
        } catch (Exception e) {
            customAnswer.setErrorMessage(e.getMessage());
            return new ResponseEntity<>(customAnswer, HttpStatus.BAD_REQUEST);
        }

        customAnswer.setSession(headerInformation.getSession());

        try {
            userService.delete(pseudoUser);
            return new ResponseEntity<>(customAnswer, HttpStatus.OK);
        } catch (Exception ex) {
            Logger.getLogger(UserControllerImpl.class.getName()).log(Level.SEVERE, null, ex);
            customAnswer.setErrorMessage(ex.getMessage());
            return new ResponseEntity<>(customAnswer, HttpStatus.INTERNAL_SERVER_ERROR);
            
        }

    }

}
