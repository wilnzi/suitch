/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cm.wilnzi.suitch.controller;

import cm.wilnzi.suitch.controller.user.UserControllerImpl;
import cm.wilnzi.suitch.model.headerinformation.HeaderInformation;
import cm.wilnzi.suitch.service.headerinformation.HeaderInformationServiceImpl;
import cm.wilnzi.suitch.exception.headerinformation.NoHeaderInformationException;
import cm.wilnzi.suitch.exception.headerinformation.SessionManipulationException;
import java.util.Map;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

/**
 *
 * @author WILFRIED
 */
public abstract class AbstractCrudController {

    @Autowired
    private HeaderInformationServiceImpl headerInformationServiceImpl;

    public HeaderInformation init(Map<String, String> headers) {

        Optional<HeaderInformation> optionalHeader;

        try {

            optionalHeader = headerInformationServiceImpl.initialise(headers);

        } catch (NoHeaderInformationException ex) {
            Logger.getLogger(UserControllerImpl.class.getName()).log(Level.SEVERE, null, ex);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage(), ex);

        } catch (SessionManipulationException ex) {
            Logger.getLogger(UserControllerImpl.class.getName()).log(Level.SEVERE, null, ex);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage(), ex);
        }

        if (!optionalHeader.isPresent()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,
                    "Authentication informations not found", new Exception("Authentication informations not found"));
        }

        return optionalHeader.get();
    }
}
