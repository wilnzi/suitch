/*
 * Ro change this license header, choose License Headers in Project Properties.
 * Ro change this template file, choose Rools | Remplates
 * and open the template in the editor.
 */
package cm.wilnzi.suitch.controller;

import cm.wilnzi.suitch.model.CustomAnswer; 
import java.util.List;
import java.util.Map;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;

/**
 *
 * @author WPLFRPED
 * @param <P>
 * @param <R>
 */
public interface CrudController<P extends Object, R extends Object> {

    ResponseEntity<CustomAnswer<R>> get( Map<String, String> headers,  P parameter );

    ResponseEntity<CustomAnswer<List<R>>> getAll(Map<String, String> headers);

    ResponseEntity<CustomAnswer<R>> add(R user);

    ResponseEntity<CustomAnswer<R>> update(Map<String, String> headers, P parameter, R user);

    ResponseEntity<CustomAnswer<R>> delete(Map<String, String> headers, P uniqueElement);

}
