/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cm.wilnzi.suitch.service.user;

import cm.wilnzi.suitch.utils.Utils;
import cm.wilnzi.suitch.model.user.User; 
import cm.wilnzi.suitch.repository.user.UserRepository; 
import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author WILFRIED
 */
@Service
public class UserServiceImpl extends UserService {
    
    @Autowired
    UserRepository userRepository;
     
    
    
    @Override
    public User get(String pseudo) {
        return userRepository.findByPseudoAndActivationStatus(pseudo, 1);
    }
    
    @Override
    public List<User> getAll() {
        return userRepository.findAll();
    }
    
    @Override
    public User add(User user) {
        user.setPassword(Utils.hash(user.getPassword()));
        return userRepository.save(user);
    }
    
    @Override
    public User update(String pseudo, User user) throws Exception {
        if (!user.getPseudo().equalsIgnoreCase(pseudo)) {
            throw new Exception("Pseudo passé en paramètre différent du pseudo utilisateur");
        }
        User oldUser = userRepository.findByPseudoAndActivationStatus(pseudo, 1);
        if (oldUser == null) {
            throw new Exception("Utilisateur non existant!!");
        }
        if (!oldUser.getPassword().equals(pseudo)) {
            user.setPassword(Utils.hash(user.getPassword()));
        }
        user.getPerson().setDisplayName();
        return userRepository.save(user);
    }
    
    @Override
    public void delete(@NotNull @Valid String pseudoUser) throws Exception {
       
        if(userRepository.findByPseudoAndActivationStatus(pseudoUser, 1) == null){
            throw new Exception("Utilisateur non existant, impossible de le supprimer");
        }  
        userRepository.customDelete(pseudoUser);
    }
    
}
