/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cm.wilnzi.suitch.service.headerinformation;

import cm.wilnzi.suitch.model.headerinformation.HeaderInformation;
import cm.wilnzi.suitch.exception.headerinformation.NoHeaderInformationException;
import cm.wilnzi.suitch.exception.headerinformation.SessionManipulationException;
import java.util.Map; 
import java.util.Optional;

/**
 *
 * @author WILFRIED
 */

 
public interface HeaderInformationService {
    
    Optional<HeaderInformation> initialise(Map<String, String> headers) throws NoHeaderInformationException, SessionManipulationException;
    
    
}
