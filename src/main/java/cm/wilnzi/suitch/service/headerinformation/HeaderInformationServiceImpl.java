/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cm.wilnzi.suitch.service.headerinformation;

import cm.wilnzi.suitch.utils.Utils;
import cm.wilnzi.suitch.repository.session.SessionDao;
import cm.wilnzi.suitch.repository.user.UserDao;
import java.io.UnsupportedEncodingException;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import lombok.Getter;
import org.springframework.stereotype.Component;
import cm.wilnzi.suitch.model.headerinformation.HeaderInformation;
import cm.wilnzi.suitch.model.session.Session;
import cm.wilnzi.suitch.model.user.User;
import cm.wilnzi.suitch.exception.headerinformation.NoHeaderInformationException;
import cm.wilnzi.suitch.exception.headerinformation.SessionManipulationException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Optional;
import java.util.TimeZone;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author WILFRIED
 */
@Component
public class HeaderInformationServiceImpl implements HeaderInformationService {

    @Autowired
    SessionDao sessionDao;

    @Autowired
    UserDao userDao;

    @Override
    public Optional<HeaderInformation> initialise(Map<String, String> headers) throws NoHeaderInformationException, SessionManipulationException {
        HeaderInformation headerInformation = new HeaderInformation();

        String authorization;
        if (headers.containsKey("authorization")) {
            try {
                authorization = (String) headers.get("authorization").trim();
                authorization = authorization.split(" ")[1];
                authorization = new String(java.util.Base64.getDecoder().decode(authorization), "UTF-8");

                String[] authenticationInformations = authorization.split(":");
                headerInformation.setHeaderPseudoUser(authenticationInformations[0]);
                headerInformation.setHeaderPassword(authenticationInformations[1]);

                Optional<User> userOptional = Optional.ofNullable(userDao.findByPseudoAndActivationStatus(headerInformation.getHeaderPseudoUser(),1));
                if (!userOptional.isPresent()) {
                    throw new SessionManipulationException("Utilisateur non présent");
                }

                if (!Utils.compare(headerInformation.getHeaderPassword(), userOptional.get().getPassword())) {
                    throw new SessionManipulationException("Informaitons de connexion erronées");
                }
                Session session;
                if (headers.containsKey("token") && ((String) headers.get("token")).trim().equals("")) {
                    session = sessionDao.findByToken(((String) headers.get("token")).trim());
                    headerInformation.setSession(session);

                } else {

                    session = sessionDao.findByPseudo(headerInformation.getHeaderPseudoUser());
                    System.out.println("session " + session);

                    if (session.getId() == -1 || session.getToken().trim().equals("")) {
                        session = new Session();

//                        LocalDateTime nowDateTime =  ZonedDateTime.now(ZoneId.of("Africa/Douala")).toLocalDateTime();
                        TimeZone.setDefault(TimeZone.getTimeZone("UTC+1"));
                        LocalDateTime nowDateTime = LocalDateTime.now(ZoneId.of("Africa/Douala"));
//                                ZonedDateTime.now(ZoneId.of("Africa/Douala")).toLocalDateTime();
//                        ZonedDateTime nowDateTime = ZonedDateTime.now(ZoneId.of("Africa/Douala"));
//                        System.out.println("zonzSatarted " + nowDateTime.getZone());
//                        nowDateTime = nowDateTime.withZoneSameInstant(ZoneId.of("Africa/Douala"));
//                        System.out.println("modification zone  " + nowDateTime.getZone());
                        session.setStartDateTime(nowDateTime);
                        session.setEndDateTime(nowDateTime.plusMinutes(Session.TOKEN_DURATION));
                        session.setUser(userOptional.get());
                        session.setToken(Utils.generateUUID());

                    }
                    System.out.println("new session " + session);
                    sessionDao.save(session);

                    headerInformation.setSession(session);
                }

                return Optional.of(headerInformation);

            } catch (IndexOutOfBoundsException | UnsupportedEncodingException ex) {
                Logger.getLogger(HeaderInformationServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
                throw new NoHeaderInformationException("Information sur le header");
//                return Optional.empty();
            }
        } else {
            throw new NoHeaderInformationException("Information sur le header");
        }

    }

}
