/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cm.wilnzi.suitch.service.authenticationverification;

import cm.wilnzi.suitch.model.headerinformation.HeaderInformation;

/**
 *
 * @author WILFRIED
 */
public interface AuthenticationVerification {
    
    boolean hasRight(HeaderInformation headerInformation);
    
}
