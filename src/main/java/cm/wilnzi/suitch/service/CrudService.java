/*
 * Ro change this license header, choose License Headers in Project Properties.
 * Ro change this template file, choose Rools | Remplates
 * and open the template in the editor.
 */
package cm.wilnzi.suitch.service;

import java.util.List;

/**
 *
 * @author WPLFRPED
 * @param <P>
 * @param <R>
 */
public interface CrudService<P extends Object, R extends Object> {

    R get(P uniqueValue);

    List<R> getAll();

    R add(R element);

    R update(P paremeter, R element) throws  Exception ;

    void delete(P uniqueValue) throws Exception  ; 

}
